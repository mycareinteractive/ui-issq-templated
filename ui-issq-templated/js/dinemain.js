var DineMain = View.extend({

    id: 'dinemain',

    template: 'dinemain.html',

    css: 'dinemain.css',

    pageSize: 6,

    subPageSize: 6,

    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
	navigate: function (key, e) {
        window.groupclicked = false;
		
		
		if(key == 'MENU') {
			this.openDineExit(this.className, this.breadcrumb);	
			return true;
		}	
		
        var navkeys = ['UP', 'DOWN', 'LEFT', 'RIGHT', 'ENTER', 'CHUP', 'CHDN', 'PGUP', 'PGDN'];
        var keyIndex = navkeys.indexOf(key);
        if (keyIndex == -1)
            return false;

        var processed = false;
        var $focusedGroup = this.$('.selected');
        var $nextGroup = $();
        var $focusedRecipe = this.$('.active');
        var $nextRecipe = $();
        var recipeSelected = ($focusedRecipe.length >= 1);
		//Current selection is on the groups
		if (!recipeSelected) {
            if (key == 'UP' || key == 'DOWN') {
                this.changeFocus(key, '.major', '', '.selected');
				var $focusedGroup = this.$('.selected');
				if(!$focusedGroup.hasClass('disabled')) {
					var groupID = $focusedGroup.attr('title');				
					this.recipesXML = this.getRecipes(groupID);
					var recipesHTML = this._buildRecipes(this.recipesXML);												
				} else {					
					this.changeFocus(key, '.major', '', '.selected');
					var $focusedGroup = this.$('.selected');
					if(!$focusedGroup.hasClass('disabled')) {
						var groupID = $focusedGroup.attr('title');				
						this.recipesXML = this.getRecipes(groupID);
						var recipesHTML = this._buildRecipes(this.recipesXML);												
					}
				}
                processed = true;
            }
            else if (key == 'CHUP' || key == 'PGUP') {
                this.focusPrevPage('.major', 9, null, 'vertical', '', '.selected');
				var $focusedGroup = this.$('.selected');
				var groupID = $focusedGroup.attr('title');				
				this.recipesXML = this.getRecipes(groupID);
				var recipesHTML = this._buildRecipes(this.recipesXML);												
                processed = true;
            }
            else if (key == 'CHDN' || key == 'PGDN') {				
                this.focusNextPage('.major', 9, null, 'vertical', '', '.selected');
				var $focusedGroup = this.$('.selected');
				var groupID = $focusedGroup.attr('title');				
				this.recipesXML = this.getRecipes(groupID);
				var recipesHTML = this._buildRecipes(this.recipesXML);												

                processed = true;
            }
            else if (key == 'RIGHT' && !$focusedGroup.hasClass('vieworder-button')) {
                this.changeFocus(key, '.minor', '', '.active');				
				this.$('.minor .sub-text1').addClass('recipesactive')				
				var $focusedRecipe = this.$('.active');
				var focusedRecipeId = $focusedRecipe.attr("id");
				var nutrientsHTML = this._buildNutrients(focusedRecipeId);				
				this.$('.sub-nutrients').show();
                processed = true;
            }
			else if (key == 'ENTER' && $focusedGroup.hasClass('vieworder-button')) {
				this.openDineView(this.className, this.breadcrumb, this.patronMenuXML, this.nutrientsXML);	
				processed = true;
			}
			else if (key == 'ENTER' && $focusedGroup.hasClass('exitdine-button')) {				
				this.openDineExit(this.className, this.breadcrumb);	
				processed = true;
			}
            else if (key == 'ENTER') {
                this.changeFocus(key, '.minor', '', '.active');
                $focusedGroup.addClass('selected');
				this.$('.minor .sub-text1').addClass('recipesactive')				
				var $focusedRecipe = this.$('.active');
				var focusedRecipeId = $focusedRecipe.attr("id");
				var nutrientsHTML = this._buildNutrients(focusedRecipeId);				
				this.$('.sub-nutrients').show();
                processed = true;
				this.$('.sub-nutrients').show();
                window.groupclicked = true;
                processed = true;
            }
		} else {  //current selection is a recipe		
            if (key == 'UP' || key == 'DOWN') {
                this.changeFocus(key, '.minor', '', '.active');
                $focusedGroup.addClass('selected');
				this.$('.minor .sub-text1').addClass('recipesactive')
				var $focusedRecipe = this.$('.active');
				var focusedRecipeId = $focusedRecipe.attr("id");
				var nutrientsHTML = this._buildNutrients(focusedRecipeId);								
                processed = true;
            }
            else if (key == 'CHUP' || key == 'PGUP' || key == 'RIGHT') {
                this.focusPrevPage('.minor .sub-text1', 8, null, 'vertical', '', '.activfse');
                $focusedGroup.addClass('selected');
                processed = true;
            }
            else if (key == 'CHDN' || key == 'PGDN') {
                this.focusNextPage('.minor .sub-text1', 8, null, 'vertical', '', '.active');
                $focusedGroup.addClass('selected');
                processed = true;
            }
            else if (key == 'LEFT') {				
                $focusedRecipe.removeClass('active');
				this.$('.minor .sub-text1').removeClass('recipesactive')
				this.$('.minor .sub-text1 a span').removeClass('active')
				this.$('.sub-nutrients').hide();
                processed = true;
            }
            else if (key == 'ENTER') {
                processed = this.click($focusedRecipe);
            }
        }
						var $focusedGroup = this.$('.selected');

						if($focusedGroup.hasClass("vieworder-button")||$focusedGroup.hasClass("exitdine-button"))
					this.$('.minor .sub-header').hide()
				else
					this.$('.minor .sub-header').show()


        return processed;
    },

    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function ($jqobj) {
		var context = this;        
        var linkid = $jqobj.attr('id');
		var groupName = $jqobj.attr('title');
        var $currGroup = $("#dinemain .major a.selected");
        var $currRecipe = $("#dinemain .minor a.active");
		
		if ($jqobj.hasClass('exitdine')) { // back button
		        
		}
				
        if (window.groupclicked) {
            window.groupclicked = false;
            return true;
        }

		// selecting a group
        if ($jqobj.hasClass('group')) {
            context.$('#.major a.selected').removeClass('selected');
            $jqobj.addClass('selected');			
            context._buildRecipes(groupName);
            return true;
        }

		
        // selecting a recipe
        if ($jqobj.hasClass('recipe')) {            
			this._orderRecipe($currRecipe)
            return true;
        }

        return false;
    },


    renderData: function () {
		//Get current meal already selected
		window.meals = window.meals || new Array();
		window.totals = window.totals || new Array();
		window.totalPrice = window.totalPrice || new Array();
        
		this.$('.page-title').html(this.breadcrumb);
        var submenu = this.className;
		
		if(submenu == 'mymenu')
			var c = $('<p class="text-large"></p>').html('My Menu');
		else 
			var c = $('<p class="text-large"></p>').html('Visitors Menu');
        
		this.$('#heading2').append(c);		
		var patronMenuXML = this.patronMenuXML;             
		this.groupsXML =  this.getGroups(patronMenuXML);		
		var groupsHTML = this._buildGroups(this.groupsXML);				
		this.nutrientsXML = this.getNutrients(patronMenuXML);		
		
		var $firstGroup = this.$('.major a:nth-child(3)');
		var groupName = $firstGroup.attr('title')		
		
		this.recipesXML = this.getRecipes(groupName);			
		var recipesHTML = this._buildRecipes(this.recipesXML);					
			
    },

    shown: function () {
        var $firstObj = this.$('.major a:nth-child(3)');
        $firstObj.click();
    },

    refresh: function (entryId) {
        //TODO: refresh status of only one entry without rebuilding the recipes        
		var context = this;        
		var $viewOrderButton = this.$('a.vieworder-button');
		var $exitDineButton = this.$('a.exitdine-button');
		if(window.meals.length == 0) 
			$viewOrderButton.addClass('disabled')
		else 
			$viewOrderButton.removeClass('disabled')

		var $focusedGroup = this.$('.selected');
		
		if($focusedGroup.hasClass('disabled')) {
			$viewOrderButton.removeClass('selected');
			$exitDineButton.addClass('selected');			
		}
    },

    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
	openDineView: function (linkid, breadcrumb, patronMenuXML, nutrientsXML) {
		
        var context = this;
		msg(linkid)		
        var page = new DineViewOrder({
            className: linkid, breadcrumb: breadcrumb, patronMenuXML: patronMenuXML, nutrientsXML: nutrientsXML, parent:this,
            oncreate: function () {                
            },
            ondestroy: function () {                
            }
        });
        page.render();
    }, 
	
	openDineMax: function (linkid, breadcrumb) {
        var context = this;
		msg(linkid)		
        var page = new DineMax({
            className: linkid, breadcrumb: breadcrumb,  
            oncreate: function () {                
            },
            ondestroy: function () {                
            }
        });
        page.render();
    }, 

	openDineExit: function (linkid, breadcrumb, patronMenuXML, nutrientsXML) {
		
		if(window.meals.length ==0) { 
			keypressed('CLOSEALL');
			keypressed(216);    //force going back to main menu
			return true;
		}			

        var context = this;
        var page = new DineExit({
            className: linkid, breadcrumb: breadcrumb, isOverlay: true,
            oncreate: function () {                
            },
            ondestroy: function () {                
            }
        });
        page.render();
    }, 

	getNutrients: function (recipe) {
		var patronMenuXML = this.patronMenuXML;
		var count 			= 0;
		var nutrients 		= '';
		var nutrient 		= '';
		var elements 		= '';
		var nutrientsDATA	= '';
		
		nutrientsDATA = nutrientsDATA+'<?xml version="1.0" encoding="ISO-8859-1" ?>';
		nutrientsDATA = nutrientsDATA+'<nutrients></nutrients>';
		
		var nutrientsXML 	= $.parseXML(nutrientsDATA);				
		$(patronMenuXML).find('RECIPES').each(function(){												  
			nutrients = $(this).find('NUTRIENTS').text();			
			currNutrients = nutrients;
			nutrient  = nutrients.split("|");			
			for(count=0;count<nutrient.length;count++)	{
				if(nutrient[count]!='')	{
					elements = nutrient[count].split("~");
					if (elements[0]=='Fiber/Dtry')
						elements[0]='Fiber';
					$(nutrientsXML).find('nutrients').append($('<nutrient title="'+elements[0]+'" measure="'+elements[2]+'" /></nutrient>'));
				}
			}			
		});	
		
		return nutrientsXML;	
	},
	
	
	getGroups: function (patronMenuXML) {
		var submenu 		= this.className;		
		var groupsDATA 		= '';
			groupsDATA 		= groupsDATA+'<?xml version="1.0" encoding="ISO-8859-1" ?>';
			groupsDATA 		= groupsDATA+'<groups></groups>';		
		var groupsXML 		= $.parseXML(groupsDATA);		
		var count 			= 0;
		var group 			= '';
		var maxselections 	= 0;
		var selections 		= 0;		
		var groupArray		= Array();
		var groupCount 		= 0;
		var groupTotal 		= 0;
		var exists			= 'N';
		
		$(patronMenuXML).find('RECIPES').each(function(){																
			$(this).find('RECIPE').each(function(){												 
				group 			= $(this).attr('group');
				maxselections 	= $(this).attr('maxselections');
				if(submenu=='visitorsmenu'||maxselections == 0)
					maxselections = 99;
				
				exists = 'N';
				for(groupCount=0;groupCount<groupTotal;groupCount++)	{
					if(group==groupArray[groupCount])
						exists = 'Y';
				}				
				if (group=='(Unassigned)')
					exists = 'Y';				
				if (exists=='N')	{
					count++;					
					$(groupsXML).find('groups').append($('<group id="group'+count+'" name="'+group+'" maxselections="'+maxselections+'" selections="'+selections+'" /></groups>'));
					groupArray[groupTotal] = group;
					groupTotal++;
				}				
			});
		});							
				
		return groupsXML;
	},
	
	
		
	getRecipes: function (selection) {
		var submenu 		= this.className;		
		var groupsDATA 		= this.groupsXML;
		var groupid 		= '';
		
		$(groupsDATA).find('group').each(function(){					  
			id		= $(this).attr('id');											 
			group 	= $(this).attr('group');			
			if (id==selection)	{
				selection = group;
				groupid	  = id;
			}
		});	
		
		var patronMenuXML 	= this.patronMenuXML
		var count 			= 0;
		var id 				= '';
		var group 			= '';
		var description 	= '';
		var longdescription = '';
		var nutrients 		= '';
		var servings 		= '';
		var price	 		= '';
		var maxquantity	 	= '';
		var recipesDATA 	= '';
		
		recipesDATA = recipesDATA+'<?xml version="1.0" encoding="ISO-8859-1" ?>';
		recipesDATA = recipesDATA+'<recipes>';
		recipesDATA = recipesDATA+'</recipes>';
		
		var recipesXML 	= $.parseXML(recipesDATA);		
		$(patronMenuXML).find('RECIPES').each(function(){																
			$(this).find('RECIPE').each(function(){												 											
				group = $(this).attr('group');										
				if (group==selection)	{			
					id 				= $(this).attr('id');
					description 	= $(this).attr('enticingdescription');
					if(!description)
						description = $(this).attr('shortname');
					longdescription = $(this).attr('publishingtext');
					nutrients 		= $(this).attr('nutrients');
					servings 		= $(this).attr('numservings');	
					price			= 0.00;	
					maxquantity		= 1;
		
					if (submenu=='visitorsmenu')
						price	 	= $(this).attr('price');
					if (submenu=='visitorsmenu'||group=='Condiments')
						maxquantity = 9;
					
					count++;
					
					$(recipesXML).find('recipes').append($('<recipe id="recipe'+count+'" recipe="'+id+'" groupid="'+groupid+'" group="'+group+'" description="'+description+'" longdescription="'+longdescription+'" nutrients="'+nutrients+'" servings="'+servings+'" price="'+price+'" maxquantity="'+maxquantity+'" />'));	
				}
			});
		});	
		
		$(recipesXML).find('recipes').append($('<STATUS count="'+count+'" />'));
		return recipesXML;	
},
	
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    _buildGroups: function () {
        var context = this;
        context.$('.major').empty();
		var patronMenuXML = this.patronMenuXML
        var ewf = ewfObject();
        
		var groupsXML = this.groupsXML;
				
		var groupsString = '<a class="exitdine-button" href="#" title="exitdine" data-translate="exitdine">< Exit Dine</a>';		
		groupsString = groupsString + '<a class="vieworder-button disabled" href="#" title="vieworder" data-translate="vieworder">View Order</a>';		
        $(groupsXML).find("groups").each(function () {
            $(groupsXML).find("group").each(function () {
                var groupID = $(this).attr("id");
				var groupName = $(this).attr("name");
				var max = $(this).attr("maxselections");			
                groupsString = groupsString + '<a href="#" title = "' + groupName + '"class = "group ' + groupName + '" max="' + max + '" id="' + groupID + '">' + groupName + '</a>';
			});
		});
		
		this.$('.major').html(groupsString);		
    },

	_checkIfOrdered: function (id) {
        var items = window.meals;
		var i = 0;
		var ordered = '';
		for (var i = 0; i < items.length; i++) {
			var item = items[i];
			var itemid = item["id"];
			if (itemid == id) {
				ordered = ' ordered';
			}
		}		
		return ordered;
    },
	
    _buildRecipes: function (groupName) {		
		var context = this;
		var patronMenuXML = this.patronMenuXML;
		var recipesXML = this.recipesXML;
		var recipesString = '';
       	var cnt = 0;
		var ordered = '';
		window.grouptotal = 0;
		$(recipesXML).find("recipes").each(function () {
            $(recipesXML).find("recipe").each(function () {
                var recipeID = $(this).attr("recipe");
				var recipeName = $(this).attr("description");				
				ordered = context._checkIfOrdered(recipeID);
							
                recipesString = recipesString + '<a href="#" data-index="' + cnt + '" title="' + recipeName + '" class="recipe' + ordered +'" id="' + recipeID + '">' + '<span class="checkmark">&nbsp;</span>'+ recipeName + '<span class="thisone recipe-' +recipeID+'">&nbsp;</span>'+'</a>';
				cnt += 1;				
				if(ordered)
					window.grouptotal = window.grouptotal + 1;				
			});
		});
		
		this.$('.minor .sub-text1').html(recipesString);
		this.$('.minor .sub-text1').scrollTop(0);		
		this.$('.minor .sub-header').show();				
    },
	
	_buildNutrients: function (recipe) {	
		var submenu 		= this.className;		
		var description 	= '';
		var longdescription = '';
		var price		    = 0.00;
		var nutrients   	= '';
		var nutrient    	= '';
		var title 			= '';
		var measure   		= '';		
		var count 			= 0;
		var nutrientsHTML 	= '';
		var recipesXML 		= this.recipesXML;
		var nutrientsXML 	= this.nutrientsXML;
		var currRecipeNutrients = '';
		
		$(recipesXML).find("recipes").each(function () {
            $(recipesXML).find("recipe").each(function () {
				id				= $(this).attr('recipe');
				if(recipe==id)	{
					description 	= $(this).attr('description');
					longdescription = $(this).attr('longdescription');
					price			= $(this).attr('price');
					nutrients 		= $(this).attr('nutrients');
					currRecipeNutrients = nutrients;
					if(!nutrients || nutrients == "||||||" || nutrients == 'undefined')  
						nutrients = "0|0|0|0|0|0|"					
					nutrient    	= nutrients.split("|");
				}
			});
		});
		
		if (description!='') {
			description = longdescription;			
			price = roundNumber(price,2);
			nutrientsHTML = nutrientsHTML+'		<p class="description">'+description+'</p>';
			if (submenu=='visitorsmenu') {
				nutrientsHTML = nutrientsHTML+'		<p class="price">$'+price+'</p>';
				window.currPrice = price;
			} else {
				nutrientsHTML = nutrientsHTML+'		<p class="price">&nbsp;</p>';
				window.currPrice = '';
			}
			nutrientsHTML = nutrientsHTML+'		<p class="nutrients">';			
			$(nutrientsXML).find('nutrient').each(function(){									   													 
				title 	= $(this).attr('title');
				measure = $(this).attr('measure');
				if(title=='Calories') 
					measure='&nbsp;';
				nutrientsHTML = nutrientsHTML+'<span class="left">'+title+'</span><span class="farright">'+measure+'</span><span class="right">'+nutrient[count]+'</span>';
				count++;
			});	
		}			
		nutrientsHTML = nutrientsHTML+'</p>';
		this.currRecipeNutrients = currRecipeNutrients;		
		this.$('.minor .sub-nutrients').html(nutrientsHTML);		
	},

	_orderRecipe: function ($focusedRecipe) {
        var itemData = null;
		var currid = $focusedRecipe.attr('id');
		var desc = $focusedRecipe.attr('title');
		var $focusedGroup = this.$('.selected');
		var groupMax = $focusedGroup.attr('max')
		var group = $focusedGroup.attr('title')
        var itemIndex = $focusedRecipe.attr('data-index');
        var entries = null;		
		var recipeOrdered = $focusedRecipe.hasClass('ordered');
		var focusedRecipeId = $focusedRecipe.attr("id");		
		
		if(recipeOrdered) {
			$focusedRecipe.removeClass('ordered');			
	            var items = window.meals;
                var i = 0;
				msg(currid);
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    var id = item["id"];                    
					msg(id + ' ' + item);
                    if (id == currid) {
						var nutrients = item['nutrients'];
						if(!nutrients || nutrients == "||||||" || nutrients == 'undefined')  
							nutrients = "0|0|0|0|0|0|"					
						var nutrient  = nutrients.split("|");						
                        window.meals.splice(i, 1);
						var nutrientAmt = 0;
						var nutrientTotal = 0;
						var nutrients = this.currRecipeNutrients;
						var nutrient  = nutrients.split("|");			
						var i = 0;
						for (var i = 0; i < nutrient.length; i++) {
							nutrientAmt = nutrient[i];					
							nutrientTotal = window.totals[i] || 0;
							window.totals[i] = Number(nutrientTotal) - Number(nutrientAmt);			
						}
						window.totalPrice = Number(window.totalPrice) - Number(window.currPrice);
						window.grouptotal = window.grouptotal - 1;
					}
                }

		} else {
			if(window.grouptotal >= groupMax) {
				this.openDineMax(this.className, this.breadcrumb);	
				return;
			}
			$focusedRecipe.addClass('ordered');
			var item 			= [];
			item['id'] 			= currid;
			item['desc'] 		= desc;
			item['group'] 		= group;
			item['qty'] 		= '1';			
			item['nutrients'] 	= this.currRecipeNutrients;
			item['price'] 		= window.currPrice;
			var len 			= window.meals.length;		
			window.meals[len] 	= item;

			var nutrientAmt 	= 0;
			var nutrientTotal 	= 0;
            var nutrients 		= this.currRecipeNutrients;
			if(!nutrients || nutrients == "||||||" || nutrients == 'undefined')  
				nutrients = "0|0|0|0|0|0|"					

			var nutrient  		= nutrients.split("|");			
			var i 				= 0;
			for (var i = 0; i < nutrient.length; i++) {
				nutrientAmt = nutrient[i];					
				nutrientTotal = window.totals[i] || 0;
				window.totals[i] = Number(nutrientTotal) + Number(nutrientAmt);
			}
			window.totalPrice = Number(window.totalPrice) + Number(window.currPrice);
			window.grouptotal = window.grouptotal + 1;
		}
		
		var $viewOrderButton = this.$('a.vieworder-button');
		if(window.meals.length == 0) 
			$viewOrderButton.addClass('disabled')
		else 
			$viewOrderButton.removeClass('disabled')				
		
        return true;
    }	

});    