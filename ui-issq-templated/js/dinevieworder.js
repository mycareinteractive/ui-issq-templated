var DineViewOrder = View.extend({

    id: 'dinevieworder',

    template: 'dinevieworder.html',

    css: 'dinevieworder.css',
	
	closeall : true,

    trackPageView: false,
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function (key) {
		
		
		if(key == 'MENU') {
			this.openDineExit(this.className, this.breadcrumb);	
			return true;
		}

		var $focusedRecipe = this.$('.selected');
		
		var focusedRecipeId = $focusedRecipe.attr('id');		
        var $nextRecipe = $();
        var $focusedButton = this.$('#buttons .active');
        var $nextButton = $();
		var buttonSelected = ($focusedButton.length >= 1);
		
		var lastItemId = $("#dinevieworder .major a:last-child").attr('id');
		var $lastItem = $("#dinevieworder .major a:last-child");
		var firstItemId = $("#dinevieworder .major a:first-child").attr('id');
		var $firstItem = $("#dinevieworder .major a:first-child");
		
		var $placeOrderButton =  $("#dinevieworder #buttons #order");
		var $orderMoreButton =  $("#dinevieworder #buttons #back");
		
		window.groupclicked = false;
        var navkeys = ['UP', 'DOWN', 'LEFT', 'RIGHT', 'ENTER', 'CHUP', 'CHDN', 'PGUP', 'PGDN'];
        var keyIndex = navkeys.indexOf(key);
        if (keyIndex == -1)
            return false;
		
		if (!buttonSelected) {
			if (key == 'ENTER') {  // default link click
				return this.click($focusedRecipe);			
			} else if (focusedRecipeId == lastItemId && (key == 'DOWN' || key == 'CHDN') ) {				
				$focusedRecipe.removeClass('selected')
				this.focus($orderMoreButton);
				return true;
			} else if (focusedRecipeId == firstItemId && (key == 'UP' || key == "CHUP")) {
				$focusedRecipe.removeClass('selected') 
				this.focus($placeOrderButton);
				//this.changeFocus(key, '#buttons', '', '.active');
				return true;
			} else if (key == 'UP' || key == 'DOWN') {								
				this.changeFocus(key, '.major', '', '.selected');
				return true;
			} else if (key == 'CHUP' || key == 'PGUP') {
                this.focusPrevPage('.major', 6, null, 'vertical', '', '.selected');
                processed = true;
            } else if (key == 'CHDN' || key == 'PGDN') {
                this.focusNextPage('.major', 6, null, 'vertical', '', '.selected');
                processed = true;
            }
		} else {			
			
			if ((key == 'LEFT' || key == 'UP') && $focusedButton.hasClass('back')) {
				$focusedButton.removeClass('active')
				this.changeFocus(key, '.major', '', '.selected');					
			} else if ((key == 'RIGHT' || key == 'DOWN') && $focusedButton.hasClass('order')) {
				$focusedButton.removeClass('active')
				this.changeFocus(key, '.major', '', '.selected');
			} else if (key == 'LEFT' || key == 'RIGHT' || key == 'UP' || key == 'DOWN' ) {
				this.changeFocus(key, '#buttons', '', '.active');
				return true;				
			} else if (key == 'CHUP' || key == 'CHDN') {
				$focusedButton.removeClass('active')
				this.changeFocus(key, '.major', '', '.selected');
			}			
		}		
        return false;
    },

    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function ($jqobj) {
        var linkid = $jqobj.attr('id');
		var qty = $jqobj.attr('qty');
		var group = $jqobj.attr('group');
		
		if ($jqobj.hasClass('back')) { // back button			
            this.destroy();
            return true;
		} 

		// selecting a recipe
        if ($jqobj.hasClass('exitdine')) {            
			this.openDineExit(this.className, this.breadcrumb,$jqobj);	
            return true;
        }

		// selecting a recipe
        if ($jqobj.hasClass('order')) {            		
			this.openDineSubmit(this.className, this.breadcrumb,$jqobj);				
            return true;
        }
		
		// selecting a recipe
        if ($jqobj.hasClass('recipe')) {            
			
			this.openDineQtyChange(this.className, this.breadcrumb,$jqobj, linkid, qty, group);	
			var $focusedLink = this.$('.active');
            return true;
        }

			
        return false;
    },

    renderData: function () {
        var context = this;        
		this._buildViewOrder(0);
        	
    },


	refresh: function (currid,newqty) {
		var context = this;

		if(window.meals.length <=0) {						
			var $orderMoreButton =  $("#dinevieworder #buttons #back");
			context.focus($orderMoreButton);			
		}

		if(newqty == 0) {
			context._buildViewOrder(0);
			context.$('.major').scrollTop(0);			
			$("#dinevieworder .major").toggle().toggle();
		} else {		
			context.updateTotals();
			context.updateItem(currid,newqty);			
			$("#dinevieworder .major").toggle().toggle();
			var $curr = $('#dinevieworder a#'+currid);
			this.scrollTo($curr);
		}				
	}, 
	
	openDineExit: function (linkid, breadcrumb, patronMenuXML, nutrientsXML) {
        var context = this;
		var exit  = '';
			
        var page = new DineExit({
            className: linkid, breadcrumb: breadcrumb, isOverlay: true,
            oncreate: function () {                
            },
            ondestroy: function () {                
            }
        });
        page.render();
    }, 
	

	openDineQtyChange: function (className, breadcrumb, data, linkid, qty, group) {
        var context = this;
		msg(linkid)		
        var page = new DineQtyChange({
            className: className, breadcrumb: breadcrumb, data: data, recipeid: linkid, qty: qty, group: group, parent:this, grandparent: this.parent,
            oncreate: function () {                
            },
            ondestroy: function () {                
            }
        });
        page.render();
    }, 
	
	openDineSubmit: function (linkid, breadcrumb, data) {
		if(window.meals.length <= 0)
			return;
        var context = this;
		msg(linkid)		
        var page = new DineSubmit({
            className: linkid, breadcrumb: breadcrumb, data: data, 
            oncreate: function () {                
            },
            ondestroy: function () {                
            }
        });
        page.render();
    }, 
	
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
	updateTotals: function () {		
		var context = this;
		context.$('#content #totals').empty();
		
		var submenu = this.className;		

		var totalsHTML = '<p>';
		totalsHTML = totalsHTML+'<span>TOTALS</span>';
		var totalPrice = roundNumber(window.totalPrice,2)
		if (submenu=='visitorsmenu')
			totalsHTML = totalsHTML+'<span class="price">$' + totalPrice +'</span>';
		else 
			totalsHTML = totalsHTML+'<span class="price">&nbsp;</span>';
		var i = 0;
		msg(window.totals)
		var nutrientAmt = 0
		
		for (var i = 0; i < window.totals.length-1; i++) {
			nutrientAmt = window.totals[i]
			nutrientAmt = roundNumber(nutrientAmt,1)
			totalsHTML = totalsHTML+'<span class="nutrient">' + nutrientAmt + '</span>'; 
			msg(totalsHTML)
		}
		context.$('#content #totals').html(totalsHTML);	
	},
	
	updateItems: function () {
		var submenu = this.className;		
		var items = window.meals;
		var i = 0;
		var ordered = '';
		var recipes = '';
		for (var i = 0; i < items.length; i++) {
			var item = items[i];
			var itemid = item["id"];
			var itemdesc = item["desc"];
			var itemqty = item['qty'];
			var itemgroup = item['group'];
			var itemprice = item['price'];
			var itemnutrients = item['nutrients'];
			var itemnutrient  = itemnutrients.split("|");			
			if (i==0) 
				recipes = recipes + '<a href="#" class="recipe selected" id="' + itemid + '" qty="' + itemqty + '" group="' + itemgroup + '">';							
			else 
				recipes = recipes + '<a href="#" class ="recipe" id="' +  itemid + '" qty="'+ itemqty + '" group="' + itemgroup + '">';							
 
			recipes = recipes+'<span class="quantity">'+itemqty+'</span>';
			recipes = recipes+'<span class="description">'+itemdesc+'</span>';
			var price = itemprice * itemqty;
			price = roundNumber(price,2)
			if (submenu=='mymenu')
			recipes = recipes+'<span class="price">&nbsp;</span>';
			else
			if (submenu=='visitorsmenu')
				recipes = recipes+'<span class="price">$'+price+'</span>';

			var nutrient = '';
			var n = 0;
			
			for(n=0;n<itemnutrient.length-1;n++)	{
				nutrientV = itemnutrient[n]*itemqty;
				nutrientV = roundNumber(nutrientV,1);
				nutrientC = 'nutrient';
				if(n==0)
					nutrientC = 'calories';
				recipes = recipes+'<span class="'+nutrientC+'">'+nutrientV+'</span>';
			}
			recipes = recipes+'			</p>';				

		}
		 
		this.$('.major').html(recipes);		
		

	},
	
	
	updateItem: function (currid,qty) {
		this.$('a#'+currid).empty();
		var submenu = this.className;	
		var items = window.meals;
		var i = 0;
		var ordered = '';
		var recipe = '';
		
		var itemid = '';
		for (var i = 0; i < items.length; i++) {			
			var item = items[i];
			itemid = item["id"];
			if(itemid==currid) {
				var itemdesc = item["desc"];
				var itemqty = item['qty'];
				var itemgroup = item['group'];
				var itemprice = item['price'];
				var itemnutrients = item['nutrients'];
				var itemnutrient  = itemnutrients.split("|");			
				
				recipe = recipe+'<span class="quantity">'+itemqty+'</span>';
				recipe = recipe+'<span class="description">'+itemdesc+'</span>';
				var price = itemprice * itemqty;
				price = roundNumber(price,2)
				if (submenu=='mymenu')
				recipe = recipe+'<span class="price">&nbsp;</span>';
				else
				if (submenu=='visitorsmenu')
					recipe = recipe+'<span class="price">$'+price+'</span>';

				var nutrient = '';
				var n = 0;
				
				for(n=0;n<itemnutrient.length-1;n++)	{
					nutrientV = itemnutrient[n]*itemqty;
					nutrientV = roundNumber(nutrientV,1);
					nutrientC = 'nutrient';
					if(n==0)
						nutrientC = 'calories';
					recipe = recipe+'<span class="'+nutrientC+'">'+nutrientV+'</span>';
				}
				recipe = recipe+'			</p>';				
			}
		}
		 
		this.$('a#'+currid).attr('qty',itemqty);		
		this.$('a#'+currid).html(recipe);		
		
	},
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
	
	_buildViewOrder: function (index) {		

		var submenu 		= this.className;		
		var nutrientsDATA 	= this.nutrientsXML;
		var orderHeadersHTML = '<p>';
		orderHeadersHTML 	= orderHeadersHTML+'<span>Qty&nbsp;Item</span>';		

		
		if (submenu=='mymenu')
			orderHeadersHTML = orderHeadersHTML+'<span class="price">&nbsp;</span>';
			else
			if (submenu=='visitorsmenu')
				orderHeadersHTML = orderHeadersHTML+'<span class="price">Price</span>';		

		$(nutrientsDATA).find('nutrient').each(function(){
			titleX = $(this).attr('title');
			titleC = 'nutrient';
			if(titleX=='Calories')	{
				titleX = 'Cal.';
				titleC = 'calories';
			}
			if(titleX=='Cholestrol')	{
				titleX = 'Chol.';
			}
			orderHeadersHTML = orderHeadersHTML+'<span class="'+titleC+'">'+titleX+'</span>';
		});			
		orderHeadersHTML = orderHeadersHTML+'</p>';		
		this.$('#content #heading').html(orderHeadersHTML);			
			
		this.updateTotals();
		this.updateItems();

		return true;			
}

	 
});    