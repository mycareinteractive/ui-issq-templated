var DineQtyChange = View.extend({

    id: 'dineqtychange',

    template: 'dineqtychange.html',

    css: 'dineqtychange.css',
	
	closeall : true,

    trackPageView: false,
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function (key) {
		var $curr = $('#buttons a.active');
		var qty = this.$('#qty').text();
		if(key == 'CLOSE' || key == 'CLOSEALL' || key == 'MENU' || key == 'HOME') {            			
			return false; 			
        }
		if (key == 'LEFT' || key == 'RIGHT' ) {
            this.changeFocus(key, '#buttons', '', '.active');
            return true;
        } else if (key == 'UP' || key == 'DOWN' ) {
            this._adjustQty(key, qty);
            return true;
        }
        
        return false;
    },

    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function ($jqobj) {
        var linkid = $jqobj.attr('id');
		var qty = this.$('#qty').text();

		if ($jqobj.hasClass('cancel')) { // back button			
            this.destroy();
            return true;
		}
		
		if ($jqobj.hasClass('confirm')) { // back button			
			this._adjustTotal(this.recipeid,qty);
            this.destroy();
            return true;
		}

        return false;
    },

    renderData: function () {
        var context = this;        
        this.$('#qty').html(this.qty);
    },

	shown: function () {
		var classname = this.className;
		msg(classname)
		
    }, 

	
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
	_adjustTotal: function (currid, qty) {			
		var items = window.meals;
		var i = 0;
		var found = false;
		if(qty == 0) {
			for (var i = 0; i < items.length; i++) {
				var item = items[i];
				var id = item["id"];                    
				if (id == currid) {
					var nutrients = item['nutrients'];
					if(!nutrients || nutrients == "||||||" || nutrients == 'undefined')  
							nutrients = "0|0|0|0|0|0|"					
					var nutrient  = nutrients.split("|");			
					window.meals.splice(i, 1);
					found = true;
					var price = item["price"];                    
					var oldqty = item["qty"];
				}
			}
			if(found) {
				var nutrientAmt = 0;
				var nutrientTotal = 0;
				var nutrient  = nutrients.split("|");			
				var i = 0;				
				for (var i = 0; i < nutrient.length; i++) {
					nutrientAmt = nutrient[i]*oldqty;										
					nutrientTotal = window.totals[i] || 0;
					window.totals[i] = Number(nutrientTotal) - Number(nutrientAmt);			
				}
				var currPrice = price * oldqty;
				window.totalPrice = Number(window.totalPrice) - Number(currPrice);				
			}
		} else {		
			var oldQty = 0;
			var price = 0.00;
			var nutrients = [];
			var itemIndex = 0;
			var i = 0;
			for (var i = 0; i < items.length; i++) {
				var item = items[i];
				var id = item["id"];                    
				if (id == currid) {
					oldQty = item['qty'];			
					price = item['price'];
					nutrients = item['nutrients'];
					itemIndex = i;
					found = true
				}
			}
			if(found) {
				var nutrient  = nutrients.split("|");			
				i = 0;


				if(oldQty != qty) {								
					for (var i = 0; i < nutrient.length; i++) {
						nutrientTotal = window.totals[i] || 0;									
						nutrientAmt = nutrient[i];					
						oldNutrientTotal = nutrientAmt * Number(oldQty)						
						newNutrientTotal = nutrientAmt * Number(qty); 								
						window.totals[i] = nutrientTotal - Number(oldNutrientTotal) + Number(newNutrientTotal);					
					}
					var newPrice = price * qty;
					var oldPrice = price * oldQty;
					items[itemIndex]['qty'] = qty;					
					window.totalPrice = Number(window.totalPrice) - Number(oldPrice) + Number(newPrice);
					window.grouptotal = window.grouptotal - oldQty + qty;
				}
			}
		}
		if(this.parent && this.parent.refresh) {
			this.parent.refresh(currid,qty);			
			this.grandparent.refresh();			
		}
		


	},
	

















































	_adjustQty: function (key, qty) {
		var group = this.group;
		var quantitymax = 1; 
		if (group=='Condiments')	{
			quantitymax = 9;
		}	else	{
			var submenu = this.className;
			if (submenu=='visitorsmenu')
				quantitymax	= 9;
		}

		if (key=='DOWN')	{
			qty--;
			if (qty<=-1)	
				qty=quantitymax;
		} else if (key=='UP')	{
			qty++;
			if (qty>=quantitymax+1)	
				qty=0;
		}
		this.$('#qty').html(qty);
	}
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
	
});    