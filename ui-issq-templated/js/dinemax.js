var DineMax = View.extend({

    id: 'dinemax',

    template: 'dinemax.html',

    css: 'dinemax.css',
	
	closeall : true,

    trackPageView: false,
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function (key) {
		var $curr = $('#buttons a.active');
		if(key == 'CLOSE' || key == 'CLOSEALL' || key == 'MENU' || key == 'HOME') {            			
			return false; 			
        }
		if (key == 'ENTER') {  // default link click
            return this.click($curr);			
        } else if (key == 'LEFT' || key == 'RIGHT' || key == 'UP' || key == 'DOWN') {
            this.changeFocus(key, '#buttons', '', '.active');
            return true;
        }
        return false;
    },

    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function ($jqobj) {
        var linkid = $jqobj.attr('id');

		if ($jqobj.hasClass('close')) { // back button			
            this.destroy();
            return true;
		}
        return false;
    },

    renderData: function () {
        var context = this;        
        this.$('#content').html(this.dialogcontent);
    },

	shown: function () {
		var classname = this.className;
		msg(classname)
		
    }, 

	
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
	
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
	
});    