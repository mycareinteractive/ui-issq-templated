var DineExit = View.extend({

    id: 'dineexit',

    template: 'dineexit.html',

    css: 'dineexit.css',
	
	closeall : true,

    trackPageView: false,
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function (key) {
		var $curr = $('#buttons a.active');
		if(key == 'CLOSE' || key == 'CLOSEALL' || key == 'MENU' || key == 'HOME') {            			
			return false; 			
        }
		if (key == 'ENTER') {  // default link click
            return this.click($curr);			
        } else if (key == 'LEFT' || key == 'RIGHT') {
            this.changeFocus(key, '#buttons', '', '.active');
            return true;
        } else if (key == 'UP' || key == 'DOWN') {
            this.changeFocus(key, '#buttons', '', '.active');
            return true;
        }
        return false;
    },

    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function ($jqobj) {
        var linkid = $jqobj.attr('id');

		if ($jqobj.hasClass('yes')) { // back button
			this._unlockMenu();
            if (this.closeall == true) {				
                keypressed('CLOSEALL');
                keypressed(216);    //force going back to main menu
            }
			return true;
		} else if ($jqobj.hasClass('no')) { // back button			
            this.destroy();
            return true;
		}
        return false;
    },

	_unlockMenu : function () {

		window.meals =[];
		window.totals =[];
		window.totalPrice = '';
		window.grouptotal = '';
		window.mrn = '';
		window.roomid = '';
		window.mealid = '';
		window.mealdate = '';
		window.menuid = '';
		window.uniquehash = '';

		var submenu = this.className;	
		if (submenu=='visitorsmenu')
			return;
			
		var ewf 		= ewfObject();
		var dataobj 	= "";
		var patron	 	= ':'+ewf.unlock_patron_menu+'/hsws/unlock_patron_menu';
		var mrn 		= 'mrn='+ window.mrn;
		var mealid 		= 'mealid='+window.mealid;
		var mealdate	= 'mealdate='+window.mealdate;
				
		var args = patron+'?'+mrn+'&'+mealid+'&'+mealdate;	
		
		var url = ewf.dinehost + args;
		
		var exitDineXML = getXdXML(url, dataobj);			
				var success = 0;			
		var errormsg = '';		
		$(exitDineXML).find('STATUS').each(function(){
			success = $(this).attr('success');			
			if(success=='0')
				$(exitDineXML).find('ERROR').each(function(){
					errormsg = $(this).attr('text');		
				});
		});
		msg(errormsg);
		return;
}



    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
	
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
	
});    