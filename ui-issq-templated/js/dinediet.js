var DineDiet = View.extend({

    id: 'dinediet',

    template: 'dinediet.html',

    css: 'dinediet.css',
	
	closeall : true,

    trackPageView: false,
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function (key) {
		var $curr = $('#buttons a.active');
		if(key == 'CLOSE' || key == 'CLOSEALL' || key == 'MENU' || key == 'HOME') {            			
			return false; 			
        }
		if (key == 'ENTER') {  // default link click
            return this.click($curr);			
        } else if (key == 'LEFT' || key == 'RIGHT' || key == 'UP' || key == 'DOWN') {
            this.changeFocus(key, '#buttons', '', '.active');
            return true;
        }
        return false;
    },

    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function ($jqobj) {
        var linkid = $jqobj.attr('id');
		var parent = $jqobj.parent().attr('title');

		if ($jqobj.hasClass('back')) { // back button
            this.destroy();
            return true;
		} else {
			this.openDineInstruction(this.className, this.breadcrumb, this.patronMenuXML);	
			return true;	
		}
		
        return false;
    },

    renderData: function () { 
        var context = this;   
		var data = this.data;
		var patronXML = data;
		
		var mealXML = this.getMeal(patronXML);
		
		$(mealXML).find('MEAL').each(function(){
			meal 		= $(this).attr('meal');
			menutype 	= $(this).attr('menutype');
			menuname 	= $(this).attr('menuname');
			description = $(this).attr('description');
		});
		
		var xml = this.getPatronMenu(data);
		
		var patronMenuXML = xml;
		
		this.patronMenuXML = patronMenuXML;
						
		var mrn			= '';
		var roomid		= '';
		var menuid		= '';
		var mealid		= '';
		var mealdate	= '';
		var uniquehash	= '';

		$(patronXML).find('PATRON').each(function(){
			mrn 		= $(this).attr('mrn');
		});

		$(patronXML).find('ROOM').each(function(){
			roomid 		= $(this).attr('id');
		});

		$(patronXML).find('MEAL').each(function(){
			mealid 		= $(this).attr('id');
			mealdate 	= $(this).attr('date');
		});

		$(patronMenuXML).find('MEAL').each(function(){
			menuid 		= $(this).attr('menuid');
			uniquehash 	= $(this).attr('uniquehash');
		});
		
		window.mrn = mrn;
		window.roomid = roomid;
		window.mealid = mealid;
		window.mealdate = mealdate;
		window.menuid = menuid;
		window.uniquehash= uniquehash;
			
		if (this.className=='visitorsmenu') {			
			this.openDineInstruction(this.className, this.breadcrumb, this.patronMenuXML);				
		}
		
		var mealHTML = '		<p class="menu">Your '+meal+' Menu: '+menuname+'</p>';
		var descHTML = '		<p class="description">'+description+'</p></div>';
		this.$('#content p.menu').html(mealHTML);
		this.$('#content p.description').html(descHTML);
    },
	
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
	 

	getPatronMenu : function (patronXML)	{
		var context = this;   
		var ewf = ewfObject();
		var patientDATA = loadJSON('patient');		
		var dataobj = '';				
		
		var type 		= 'PATRONMENU';
		var mrn 		= 'mrn=';
		var mealid 		= 'mealid=';
		var mealdate	= 'mealdate=';
		var lock  		= 'lock=true';
		var nutri  		= 'nutri=R';
		var menuid 		= 'menuid=33';
	
		
		$(patronXML).find('PATRON').each(function(){
			mrn 		= mrn+$(this).attr('mrn');
		});	
		
		$(patronXML).find('MEAL').each(function(){
			mealid 		= mealid+$(this).attr('id');
			mealdate 	= mealdate+$(this).attr('date');
		});	
		
		
		if (this.className=='visitorsmenu') {
			var args = '?'+mrn+'&'+menuid+'&'+nutri;
			var main_url = ewf.dinehost + ":"+ ewf.request_guest_menu;
		} else {
			var args = '?'+mrn+'&'+mealid+'&'+mealdate+'&'+lock+'&'+nutri;				
			var main_url = ewf.dinehost + ":"+ewf.request_patron_menu;
		}
		
		
		var url = main_url + args;						
		msg(url)
		var xml = getXdXML(url, dataobj);		
		var success = 0;	
		var errormsg = '';		
		$(xml).find('STATUS').each(function(){
			success = $(this).attr('success');			
			if(success=='0')
				$(xml).find('ERROR').each(function(){
					errormsg = $(this).attr('text');		
				});
		});

		if(success=='0') {
			msg(errormsg)
			this.openDineError(this.className, this.breadcrumb, errormsg);
			return;
		}
		
		return xml;	
	}, 

	getMeal : function (patronXML)	{
	
		var menutypes = '<?xml version="1.0" encoding="ISO-8859-1" ?> <MENUTYPES> <MENUTYPE menutype="BARIATRIC MAINTENANCE" menuname="Bariatric Maintenance" description="This menu is designed for a patient who has had bariatric surgery in the past. It allows only 3 ounces of food served at one time. Foods are low in sugar and low in fat and should be eaten at regularly spaced intervals throughout the day.  Beverages are non-carbonated." /> <MENUTYPE menutype="BARIATRIC SURGERY" menuname="Bariatric Surgery" description="This menu was designed by your doctor and dietician for post-op bariatric surgery. It consists of 4oz portions of non-carbonated clear liquids and protein supplements. To prevent swallowing air, straws are not allowed." /> <MENUTYPE menutype="BRAT" menuname="Bananas, Rice, Applesauce and Toast" description="This menu consists of Bananas, Rice, Applesauce and Toast. It is a temporary diet intended to assist in managing diarrhea in children." /> <MENUTYPE menutype="CARDIAC" menuname="Cardiac" description="Heart healthy menu based on the American Heart Association guidelines. Daily goals are less than 75g fat, 200mg cholesterol and 2000mg sodium per day.  Foods that are high in fat such as full fat dairy products, eggs, high fat meats and salty condiments are replaced with lower fat and lower sodium options.  Beverages are limited to those that do not contain caffeine or have been decaffeinated. " /> <MENUTYPE menutype="DIABETIC" menuname="Diabetic" description="This is a carbohydrate controlled menu. Items are limited in sugars, starches and saturated fats to prevent your blood sugar from getting too high. Based on your total calorie intake for the day, carbohydrate servings will be 45-75g per meal. " /> <MENUTYPE menutype="DYSPHAGIA ADVANCED" menuname="Dysphagia Advanced" description="Dysphagia means difficulty swallowing. Foods on this menu are chopped and moistened with added sauce or gravy. Liquids may be thickened for safer swallowing." /> <MENUTYPE menutype="DYSPHAGIA MECH. ALTERED" menuname="Dysphagia Mechanically Altered" description="Dysphagia means difficulty swallowing. Foods on this menu are minced, ground or pureed and moistened with added sauce or gravy. Bread-products and rice are not allowed, as well as hard, sticky, or crunchy foods. Liquids may be thickened for safer swallowing." /> <MENUTYPE menutype="DYSPHAGIA PUREED" menuname="Dysphagia Pureed" description="Dysphagia means difficulty swallowing. Foods on this menu are pureed to pudding-like consistency. They are moistened with added sauce or gravy. Liquids may be thickened for safer swallowing. " /> <MENUTYPE menutype="FINGER FOODS" menuname="Finger Foods" description="Foods on this menu do not require use of utensils. They are easy to pick up and eat with your fingers." /> <MENUTYPE menutype="GENERAL" menuname="General" description="The General menu offers our full selection of choices. Order a variety of foods from each food group: grains, fruits, vegetables, dairy, meats and beans. Limit excessive sweets and extra fat." /> <MENUTYPE menutype="GLUTEN FREE" menuname="Gluten Free" description="This Gluten Free menu does not contain wheat, rye, barley or oat products or ingredients. Foods are prepared using separate utensils and a separate toaster to avoid contamination." /> <MENUTYPE menutype="KETOGENIC" menuname="Ketogenic" description="This specialized menu is customized for you by your dietitian. Generally it is very high in fats and very low in carbohydrate foods." /> <MENUTYPE menutype="KOSHER" menuname="Kosher" description="This menu follows the Jewish Dietary Laws. Approved items include pre-packaged meals and snacks." /> <MENUTYPE menutype="LIQUID" menuname="Liquid" description="This menu includes items that are clear liquids such as broth, tea, jell-o and popsicles and full liquids such as soups, yogurt, pudding and ice cream. Full liquids must be liquid at room temperature or body temperature and have a smooth consistency. " /> <MENUTYPE menutype="LOW FAT" menuname="Low Fat" description="The Lowfat menu limits egg yolks, full fat dairy products and high fat meats to maintain 30% or less of your daily calories from fat. Goals are 65 grams of fat or less and 200mg of dietary cholesterol per day. " /> <MENUTYPE menutype="LOW IODINE" menuname="Low Iodine" description="The Low Iodine menu limits foods rich in iodine including seafood, dairy products, soy products and most processed foods." /> <MENUTYPE menutype="LOW RESIDUE" menuname="Low Residue" description="This menu emphasizes foods that are low in fiber and limits whole grains, raw or fibrous fruits, vegetables, beans, nuts and seeds." /> <MENUTYPE menutype="PEDIATRIC" menuname="Pediatric" description="A menu for Pre-School and school-age children. Consists of foods appropriate for age level greater than 3 years, including finger foods." /> <MENUTYPE menutype="RENAL" menuname="Renal" description="A menu to help you manage the effects of kidney disease. Daily intakes of sodium containing foods such as condiments containing salt, salty meats and cheeses and potassium containing foods such as bananas, oranges and potatoes are limited to 2000 mg or less per day.  Phosphorus which is found in foods including sodas and dairy products may be also limited. " /> <MENUTYPE menutype="SODIUM CONTROLLED" menuname="Sodium Controlled" description="This menu contains meals and snacks that provide a total of less than 2000mg sodium per day.  Foods that have added salt such as salad dressings and high sodium meats and cheeses are replaced with lower sodium options." /> <MENUTYPE menutype="SUBSTITUTE" menuname="Substitute" description=" " /> <MENUTYPE menutype="TF" menuname="Tube Feeding" description=" " /> <MENUTYPE menutype="NPO" menuname="NPO" description=" " /> <STATUS success="1" /> </MENUTYPES>';	
		
		var count 		= 1;
		var meal 		= '';
		var menutype 	= '';
		var menuname 	= '';
		var description = 'Dietary description not available.';
		var mealXML 	= '';
		
		mealXML = mealXML+'<?xml version="1.0" encoding="ISO-8859-1" ?>';
		mealXML = mealXML+'<MEALS>';
		
		$(patronXML).find('MEAL').each(function(){
			meal 		= $(this).attr('name');
			menutype 	= $(this).attr('menuname');
		});
				
		$(menutypes).find('MENUTYPE').each(function(){
			if (menutype == $(this).attr('menutype'))	{
				menuname 	= $(this).attr('menuname');
				description = $(this).attr('description');
			}
		});
		
		mealXML = mealXML+'<MEAL meal="'+meal+'" menutype="'+menutype+'" menuname="'+menuname+'" description="'+description+'" />';
		mealXML = mealXML+'<STATUS count="'+count+'" />';
		mealXML = mealXML+'</MEALS>';
			

		return mealXML;
	},

	
	openDineError: function (linkid, breadcrumb, data, pagePath) {
        var context = this;
		
        var page = new DineError({
            className: linkid, breadcrumb: breadcrumb, data: data, pagePath: pagePath,
            oncreate: function () {                
            },
            ondestroy: function () {                
            }
        });
        page.render();
    },
	
	openDineInstruction: function (linkid, breadcrumb, patronMenuXML, pagePath) {
        var context = this;
        var page = new DineInstructions({
            className: linkid, breadcrumb: breadcrumb, patronMenuXML: patronMenuXML, pagePath: pagePath,
            oncreate: function () {                
            },
            ondestroy: function () {                
            }
        });
        page.render();
    } 



    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
	
});    