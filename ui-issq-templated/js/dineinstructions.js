var DineInstructions = View.extend({

    id: 'dineinstructions',

    template: 'dineinstructions.html',

    css: 'dineinstructions.css',
	
	closeall : true,

    trackPageView: false,
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function (key) {
		var $curr = $('#buttons a.active');
		if(key == 'CLOSE' || key == 'CLOSEALL' || key == 'MENU' || key == 'HOME') {            			
			return false; 			
        }
		if (key == 'ENTER') {  // default link click
            return this.click($curr);			
        } else if (key == 'LEFT' || key == 'RIGHT') {
            this.changeFocus(key, '#buttons', '', '.active');
            return true;
        } else if (key == 'UP' || key == 'DOWN') {
            this.changeFocus(key, '#buttons', '', '.active');
            return true;
        }
        return false;
    },

    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function ($jqobj) {
        var linkid = $jqobj.attr('id');

		if ($jqobj.hasClass('mainmenu')) { // back button
            if (this.closeall == true) {				
                keypressed('CLOSEALL');
                keypressed(216);    //force going back to main menu
            }
			return true;
		} else if ($jqobj.hasClass('back')) { // back button
            this.destroy();
            return false;
		} else {
			this.openDineMain(this.className, this.breadcrumb, this.patronMenuXML);	
			return true;	
		} 
        return false;
    },

    renderData: function () {
        var context = this;        		   
    },

	shown: function () {
		var classname = this.className;
		msg(classname)
		if(classname == 'mymenu') {
			this.$('p.indent.visitormenu').hide();
		}
		
    }, 

	
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
	openDineMain: function (linkid, breadcrumb, patronMenuXML, pagePath) {
        var context = this;
		msg(linkid)		
        var page = new DineMain({
            className: linkid, breadcrumb: breadcrumb, patronMenuXML: patronMenuXML, pagePath: pagePath,
            oncreate: function () {                
            },
            ondestroy: function () {                
            }
        });
        page.render();
    }
	
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
	
});    