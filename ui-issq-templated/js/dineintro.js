var DineIntro = View.extend({

    id: 'dineintro',

    template: 'dineintro.html',

    css: 'dineintro.css',
	
	closeall : true,

    trackPageView: false,
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function (key) {
		var $curr = $('#buttons a.active');
		if(key == 'CLOSE' || key == 'CLOSEALL' || key == 'MENU' || key == 'HOME') {            			
			return false; 			
        }
		
		if (key == 'ENTER') {  // default link click
            return this.click($curr);			
        } else if (key == 'LEFT' || key == 'RIGHT' || key == 'UP' || key == 'DOWN') {
            this.changeFocus(key, '#buttons', '', '.active');
            return true;
        }
        return false;
    },

    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function ($jqobj) {
        var linkid = $jqobj.attr('id');
		var parent = this.data.breadcrumb;
		msg('linkid ' + linkid + ' ' + parent);
		var data = this.data;
		if ($jqobj.hasClass('back') || $jqobj.hasClass('neither') || $jqobj.hasClass('mainmenu')) { // back button
			if(this.closeall==true) {			
				keypressed('CLOSEALL');
				keypressed(216);    //force going back to main menu
			}
            this.destroy();
            return true;
        } else if (linkid == 'mymenu-button') {		
			this.getPatronInfo('mymenu', parent, data);
			return true;	
		} else if (linkid == 'visitorsmenu-button') {
			this.getPatronInfo('visitorsmenu', parent, data);
			return true;	
		} 
        return false;
    },

    renderData: function () { 
        var context = this;   
		var data = this.data;
		
		this.$('#content p.greeting').html('Hello, ' + data.patientname);
		this.$('#buttons a#mymenu-button').html(data.patientname);
		this.$('#buttons a#visitorsmenu-button').html('A visitor of ' + data.patientname);        
    },

	shown: function () {
		var data = this.data;
		if(data.type == 'mymenu') 
			var $firstObj = this.$('#buttons a:nth-child(1)');
		else if(data.type == 'visitormenu') 
			var $firstObj = this.$('#buttons a:nth-child(2)');
        $firstObj.addClass('active');
    },
	
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
	getPatronInfo: function(linkid, breadcrumb, data, pagePath) {
		
		var context = this;   
		var ewf = ewfObject();
		
		var patientDATA = loadJSON('patient');		
	    var data = this.data;		
	   	
		var deviceID = window.settings.mac;		
		var name 	= '?name='+patientDATA.userFullName;
		var room 	= 'room='+patientDATA.roomNumber;
		var bed  	= 'bed='+patientDATA.roomBed;
		var mac  	= 'mac='+deviceID;
		var args 	= name+'&'+room+'&'+bed+'&'+mac;
		
        var dataobj = '';
                
		this.$('#mask').show();
		
		nova.tracker.event('dine', 'start', window.settings.homeID);
		var context = this;
		var main_url = ewf.dinehost + ":"+ ewf.getPatron;
		url = main_url + args;						
		
		var patronXML = getXdXML(url, dataobj);		
		var success = 0;	
		var errormsg = '';		
		$(patronXML).find('STATUS').each(function(){
			success = $(this).attr('success');			
			if(success=='0')
				$(patronXML).find('ERROR').each(function(){
					errormsg = $(this).attr('text');		
				});
		});

		if(success=='0') {
			msg(errormsg)
			this.openDineError(this.className, this.breadcrumb, errormsg);
			return;
		}
		

		if(data.type == 'mymenu') {
			var isValidPatron = this.validatePatron(patronXML);			
			if(isValidPatron.indexOf('ERROR')>=0) {
				this.openDineError(this.linkid, this.breadcrumb, isValidPatron);
				return;
			}
		}				
				
		this.openDineDiet(linkid, this.breadcrumb, patronXML);	
        return true;		
    },
	
	validatePatron: function (patronXML)	{
	
		var patientDATA = loadJSON('patient');		
		
		var nameI	= patientDATA.userFullName;
		var roomI	= patientDATA.roomNumber;
		var nameO	= '';
		var roomO	= '';
		var isValidPatron = 'true';
		var hasOrdered     = '';
			
		$(patronXML).find('PATRON').each(function(){
			nameO 	= $(this).attr('firstname')+' '+$(this).attr('lastname');
		});	
		
		$(patronXML).find('MEAL').each(function(){
			meal 		= $(this).attr('name');
			menutype 	= $(this).attr('menuname');
		});
		
		$(patronXML).find('ROOM').each(function(){
			roomO 	= $(this).attr('name').substr(0,4);
		});	

		$(patronXML).find('MEAL').each(function(){
			hasOrdered 	= $(this).attr('hasselections');
		});
		
		if (nameI!=nameO||roomI!=roomO)				
			isValidPatron = 'ERROR: patron call and returned information conflict... '+' name IN: '+nameI+' name OUT: '+nameO+' room IN: '+roomI+' room OUT: '+roomO;
		else if (hasOrdered=='1')	
			isValidPatron = 'ERROR: patient has already ordered for this meal';
		msg(isValidPatron)
		return isValidPatron;
	},
	
	openDineError: function (linkid, breadcrumb, data, pagePath) {
        var context = this;
		
        var page = new DineError({
            className: linkid, breadcrumb: breadcrumb, data: data, pagePath: pagePath,
            oncreate: function () {                
            },
            ondestroy: function () {                
            }
        });
		pause(3000);
		this.destroy();
        page.render();
    }, 
	
	openDineDiet: function (linkid, breadcrumb, data, pagePath) {
        var context = this;
		msg(linkid)		
        var page = new DineDiet({
            className: linkid, breadcrumb: breadcrumb, data: data, pagePath: pagePath,
            oncreate: function () {                
            },
            ondestroy: function () {                
            }
        });
        page.render();
    }
	
		
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/

});    