var DineSubmit = View.extend({

    id: 'dinesubmit',

    template: 'dinesubmit.html',

    css: 'dinesubmit.css',
	
	closeall : true,

    trackPageView: false,
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function (key) {
		var $curr = $('#buttons a.active');
		if(key == 'CLOSE' || key == 'CLOSEALL' || key == 'MENU' || key == 'HOME') {            			
			return false; 			
        }
		if (key == 'ENTER') {  // default link click
            return this.click($curr);			
        } else if (key == 'LEFT' || key == 'RIGHT') {
            this.changeFocus(key, '#buttons', '', '.active');
            return true;
        } else if (key == 'UP' || key == 'DOWN') {
            this.changeFocus(key, '#buttons', '', '.active');
            return true;
        }
        return false;
    },

    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function ($jqobj) {
        var linkid = $jqobj.attr('id');

		if ($jqobj.hasClass('yes')) { 

			this._submitOrder(this.className, this.breadcrumb,$jqobj);	
			
			return true;
		} else if ($jqobj.hasClass('no')) { // back button			
            this.destroy();
            return true;
		}
        return false;
    },

    renderData: function () {
        var context = this;        
        this.$('#content').html(this.dialogcontent);
    },

	shown: function () {
		var classname = this.className;
		msg(classname)
		
    }, 

		openDineError: function (linkid, breadcrumb, data, pagePath) {
        var context = this;
		
        var page = new DineError({
            className: linkid, breadcrumb: breadcrumb, data: data, pagePath: pagePath,
            oncreate: function () {                
            },
            ondestroy: function () {                
            }
        });
        page.render();
    },
		
	openDineSubmitted: function (linkid, breadcrumb, data) {
        var context = this;
	
		msg(linkid)		
        var page = new DineSubmitted({
            className: linkid, breadcrumb: breadcrumb, data: data, 
            oncreate: function () {                
            },
            ondestroy: function () {                
            }
        });
        page.render();
    }, 
	
    
	
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
	
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
	_submitOrder : function (className, breadcrumb, $obj) {				
		//var errormsg = '';
		//this.openDineError(this.classname, this.breadcrumb, errormsg);
		//return;
		
		var ewf 		= ewfObject();
		var dataobj = '';				
		
		// BUILD ORDER
				
		var mrn			= window.mrn;
		var roomid		= window.roomid;
		var menuid		= window.menuid;
		var mealid		= window.mealid;
		var mealdate	= window.mealdate;
		var uniquehash	= window.uniquehash;
		
		var selectionsXML   = '';
		
		selectionsXML = selectionsXML+'<?xml version="1.0" encoding="ISO-8859-1" ?>';	
		selectionsXML = selectionsXML+'<PATRONSELECTIONS>';
		selectionsXML = selectionsXML+'<MRN>'+mrn+'</MRN>';
		selectionsXML = selectionsXML+'<ROOMID>'+roomid+'</ROOMID>';
		selectionsXML = selectionsXML+'<MENUID>'+menuid+'</MENUID>';
		selectionsXML = selectionsXML+'<MEALID>'+mealid+'</MEALID>';
		selectionsXML = selectionsXML+'<MEALDATE>'+mealdate+'</MEALDATE>';
		selectionsXML = selectionsXML+'<UNIQUEHASH>'+uniquehash+'</UNIQUEHASH>';
		selectionsXML = selectionsXML+'<SELECTIONS>';
		
		var items = window.meals;
		var i = 0;
		var item = '';
		var id = '';
		var numservings = 0;		
		
		for (var i = 0; i < items.length; i++) {
				item = items[i];
				id = item["id"];
				numservings = item['qty'];
				selectionsXML = selectionsXML+'<RECIPE id="'+id+'" numservings="'+numservings+'" ></RECIPE>';
		}
		
		selectionsXML = selectionsXML+'</SELECTIONS>';
		selectionsXML = selectionsXML+'</PATRONSELECTIONS>';	
			
		// SUBMIT ORDER
		
		var ewf 		= ewfObject();
		var type 		= 'SELECTMENU';	
		
		var submenu = this.className;
		var main_url = '';
		if (submenu=='visitorsmenu') {
			main_url = ewf.dinehost + ":"+ ewf.order_guest_tray+'?xmlstring=';
		} else {			
			main_url = ewf.dinehost + ":"+ewf.select_patron_menu+'?xmlstring=';
		}
			
		url = main_url + selectionsXML;								
		var submittedMealXML = getXdXML(url, dataobj);		
		var success = 0;			
		var errormsg = '';		
		$(submittedMealXML).find('STATUS').each(function(){
			success = $(this).attr('success');			
			if(success=='0')
				$(submittedMealXML).find('ERROR').each(function(){
					errormsg = $(this).attr('text');		
				});
		});

		if(success=='0') {
			msg(errormsg)
			this.openDineError(this.classname, this.breadcrumb, errormsg);
			return;
		}
		this.openDineSubmitted(className, breadcrumb,$obj);				
		}
	
});    