var DineError = View.extend({

    id: 'dineerror',

    template: 'dineerror.html',

    css: 'dineerror.css',
	
	closeall : true,

    trackPageView: false,
    /**********************************************************************************
     * Overwrite functions; from base class
     ********00000*************************************************************************/
    navigate: function (key) {
		var $curr = $('#buttons a.active');
		if(key == 'CLOSE' || key == 'CLOSEALL' || key == 'MENU' || key == 'HOME') {            			
			return false; 			
        }

		if (key == 'ENTER') {  // default link click
            return this.click($curr);			
        } 
        return false;
    },

    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function ($jqobj) {
        var linkid = $jqobj.attr('id');
		var parent = $jqobj.parent().attr('title');

		if ($jqobj.hasClass('close')) { // back button
			window.meals =[];
			window.totals =[];
			window.totalPrice = '';
			window.grouptotal = '';
			window.mrn = '';
			window.roomid = '';
			window.mealid = '';
			window.mealdate = '';
			window.menuid = '';
			window.uniquehash = '';

            if (this.closeall == true) {				
                keypressed('CLOSEALL');
                keypressed(216);    //force going back to main menu
            }
			return true;
		}
        return false;
    },

	renderData: function () { 
        var context = this;   
		var data = this.data;	
    }
	
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
	 
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
	
});    