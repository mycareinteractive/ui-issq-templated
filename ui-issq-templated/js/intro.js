var Intro = View.extend({

    id: 'intro',

    template: 'intro.html',

    css: 'intro.css',

    className: 'intro',

    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function (key) {
        var $curr = $('#intro a.active');
		if(key=='POWR')
			return false;

        var ignoredKeys = ['MENU', 'HOME', 'EXIT', 'BACK', 'POWR', 'CLOSE', 'CLOSEALL'];
        if(ignoredKeys.indexOf(key) >= 0) {
            return true;
        }
        else if (key == 'ENTER') {  // default link click		
            return this.click($curr);
        }

        return false;
    },

    click: function ($jqobj) {
        var linkid = $jqobj.attr('id');

        if (linkid == 'next') {
            // tracking
            nova.tracker.event('intro', 'next');

            // close both intro page and intro page
            window.pages.closePage('intro');
            this.destroy();

            // build primary page
            var page = new Intro2({});
            page.render();
            return true;
        }
        
        return false;
    },

    renderData: function () {
        var context = this;
    },

    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    watchTV: function (className) {
        //var context = this;
        //
        //this.tvChannels = [];
        // var channels = epgChannels().Channels;
        // $.each(channels, function(i, ch) {
        //     context.tvChannels.push({type: 'Analog', url: ch.channelNumber, display: ch.channelName});
        // });
        // var page = new VideoPlayer2({viewId: 'tvonly', className:className, playlist: this.tvChannels, playlistIndex: 0, delay: 10000, delayMessage: 'One moment while we are loading the television channels...'});
        // page.render();
        setLanguage(''); 
        var linkId = 'tv';
        var pagePath = this.pagePath + '/' + linkId;
        var page = new TVGuide({className: className, pagePath: pagePath});
        page.render();
    }
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/

});

