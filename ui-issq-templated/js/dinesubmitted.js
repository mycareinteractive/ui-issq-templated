var DineSubmitted = View.extend({

    id: 'dinesubmitted',

    template: 'dinesubmitted.html',

    css: 'dinesubmitted.css',
	
	closeall : true,

    trackPageView: false,
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
	navigate: function (key) {
		var $curr = $('#buttons a.active');
		
		if(key == 'MENU' ) {            			
			keypressed('CLOSEALL');
			keypressed(216);    //force going back to main menu
            return true;
		}

		if (key == 'ENTER') {  // default link click
            return this.click($curr);			
        }
        return false;
    },

    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function ($jqobj) {
        var linkid = $jqobj.attr('id');

		if ($jqobj.hasClass('close')) { // back button			
			window.meals =[];
			window.totals =[];
			window.totalPrice = '';
			window.grouptotal = '';
			window.mrn = '';
			window.roomid = '';
			window.mealid = '';
			window.mealdate = '';
			window.menuid = '';
			window.uniquehash = '';
		
			keypressed('CLOSEALL');
			keypressed(216);    //force going back to main menu
            return true;
		}
        return false;
    },

    renderData: function () {
        var context = this;      
		var submenu = this.className;		
		var message = '';		
		if (submenu=='mymenu')	{
		
			var patientDATA = loadJSON('patient');		
			var name		 = patientDATA.userFullName;
			if (!name)	name = 'NO NAME';		
			
			message = message+'<p>Thank you, '+name+', for your order.<br />It will be confirmed by our Diet Clerk.<br />Please call Ext. 34222 if you have any questions.<br /><span class="text-color-2">Approximate delivery time is 45 minutes.</span><br/>';
			message = message+'<br/>If you have diabetes, please let your nurse know<br />you have placed an order.</p>';
		
		} else if (submenu=='visitorsmenu')	{
			
			var totalPrice = roundNumber(window.totalPrice,2)

			message = message+'<p>Thank you for your order.<br />Your total amount due upon delivery is <span>$'+totalPrice+'</span>.<br />Please call Ext. 34222 if you have any questions.<br/><br/>';
			message = message+'<span class="text-color-2">Approximate delivery time is 45 minutes.</span></p>';
			
		}
		
		this.$('#content #message').html(message);
		
	},
   

	shown: function () {
    }

	
	
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/

    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
	
	 
});    